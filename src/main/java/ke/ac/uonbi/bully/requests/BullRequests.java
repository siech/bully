/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.ac.uonbi.bully.requests;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import ke.ac.uonbi.bully.entities.Bull;

/**
 *
 * @author siech
 */
public class BullRequests {

    public int addBull() {

        Connection connection = MysqlConnection.getConnection();

        Statement statement;
        ResultSet result;
        Bull bull;
        try {
            statement = connection.createStatement();

            statement.executeUpdate("INSERT INTO bull(active) values(" + true + ")");
            bull = new Bull();
            result = statement.executeQuery("SELECT * FROM bull ORDER BY id DESC LIMIT 1");
            result.next();
            bull.setId(result.getInt("id"));

            connection.close();
        } catch (SQLException ex) {
            System.err.println("Bull could not created");
            return 0;
        }

        return bull.getId();
    }

}
