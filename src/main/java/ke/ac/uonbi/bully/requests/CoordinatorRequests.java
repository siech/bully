/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.ac.uonbi.bully.requests;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import ke.ac.uonbi.bully.entities.Bull;
import ke.ac.uonbi.bully.entities.Election;

/**
 *
 * @author siech
 */
public class CoordinatorRequests {

    public Election startElection() {

        Connection connection = MysqlConnection.getConnection();

        ResultSet result;
        Statement statement;

        Election election;
        try {
            statement = connection.createStatement();
            election = new Election();

            statement.executeUpdate("INSERT INTO election(coordinator) VALUES (" + null + ")");
            result = statement.executeQuery("SELECT * FROM election ORDER BY id DESC LIMIT 1");
            result.next();
            election.setId(result.getInt("id"));

            election.setCoordinator(new Bull(result.getInt("coordinator")));

            connection.close();
        } catch (SQLException ex) {
            System.err.println("Election could not be started " + ex);
            return null;
        }

        return election;
    }

    public void setCoordinator(int bullId, int electionId) {
        Connection connection = MysqlConnection.getConnection();

        Statement statement;

        try {
            statement = connection.createStatement();
            statement.executeUpdate("UPDATE election SET coordinator=" + bullId + " WHERE id=" + electionId);

            connection.close();
        } catch (SQLException ex) {
            System.err.println("Coordinator could not be set "+ex);
        }

    }

}
