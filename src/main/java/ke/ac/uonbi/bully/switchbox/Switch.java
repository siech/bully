/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.ac.uonbi.bully.switchbox;

import java.util.Random;
import java.util.Scanner;
import ke.ac.uonbi.bully.bull.BullDetails;
import ke.ac.uonbi.bully.communication.Server;
import ke.ac.uonbi.bully.requests.BullRequests;

/**
 *
 * @author siech
 */
public class Switch {

    public static void main(String[] args) {
        setBulls(new BullDetails[6]);

        BullRequests bullRequests = new BullRequests();
        for (int i = 0; i < 6; i++) {
            bulls[i] = new BullDetails(bullRequests.addBull());
            bulls[i].setAlive(true);
        }
        for (int i = 0; i < 6; i++) {
            bulls[i].getServer().setBulls(bulls);
            bulls[i].getServer().initiateElection();
        }
        for (int i = 0; i < 6; i++) {
            new Thread(bulls[i].getServer()).start();
        }
        try {
            Thread.sleep(4000);
        } catch (Exception e) {
            System.err.println("Interrupted");
        }

        while (true) {
            System.out.println("\nWhat would you like to test");
            System.out.println("\t1. List live process");
            System.out.println("\t2. Initiate an election");
            System.out.println("\t3. Kill a process");
            System.out.println("\t4. Start a new process");
            System.out.println("\t5. Revive a dead process");
            System.out.println("\t6. Show the coordinator");
            System.out.println("\t`````````````````````````");
            System.out.print("\t>> ");
            int choice;
            try {
                choice = new Scanner(System.in).nextInt();
                System.out.println();
            } catch (Exception e) {
                System.err.println("Improper choice\n");
                continue;
            }
            switch (choice) {
                case 1:
                    for (int i = 0; i < bulls.length; i++) {
                        if (bulls[i].isAlive()) {
                            System.out.println("Process " + (i + 1) + " -> Bull " + bulls[i].getId());
                        }
                    }
                    System.out.println();
                    try {
                        Thread.sleep(2000);
                    } catch (Exception e) {
                        System.err.println("Interrupted\n");
                    }
                    break;
                case 2:
                    System.out.print("\nWhich bull(process) do you want to initiate the election"
                            + "\n\t(Choose between:" + getLiveChoices() + ")\n\t>> ");
                    try {
                        choice = new Scanner(System.in).nextInt();
                        System.out.println();
                        if (choice > 9) {
                            getBull(choice, bulls).getServer().initiateElection();
                            Thread.sleep(2000);
                        } else if (choice >= 1 && choice <= 6) {
                            choice -= 1;
                            bulls[choice].getServer().initiateElection();
                            Thread.sleep(2000);
                        } else {
                            System.err.println("Improper choice\n");
                            break;
                        }
                    } catch (Exception e) {
                        System.err.println("Improper choice\n");
                        break;
                    }
                    break;
                case 3:
                    System.out.print("\nWhich bull(process) do you want to kill"
                            + "\n\t(Choose between: " + getLiveChoices() + ")\n\t>> ");
                    try {
                        choice = new Scanner(System.in).nextInt();
                        System.out.println();
                        boolean coordinatorDown = false;
                        if (choice > 9) {
                            BullDetails bull = getBull(choice, bulls);
                            bull.kill();
                            if (bull.getId() == Server.getCoordinator().getId()) {
                                coordinatorDown = true;
                            }
                            System.out.println("\033[31;2mSwitchbox: Bull " + bull.getId() + " is down");
                        } else if (choice >= 1 && choice <= 6) {
                            choice -= 1;
                            bulls[choice].kill();
                            if (bulls[choice].getId() == Server.getCoordinator().getId()) {
                                coordinatorDown = true;
                            }
                            System.out.println("\033[31;2mSwitchbox: Bull " + bulls[choice].getId() + " is down");
                        } else {
                            System.err.println("Improper choice\n");
                            break;
                        }
                        Thread.sleep(2000);

                        if (coordinatorDown) {
                            while (true) {
                                int random = new Random().nextInt(bulls.length);
                                if (bulls[random].isAlive()) {
                                    System.out.println("\n\033[31;2mBull " + bulls[random].getId() + ": Coordinator is down\n");
                                    Thread.sleep(1000);
                                    bulls[random].getServer().initiateElection();
                                    Thread.sleep(2000);
                                    break;
                                }
                            }
                        }
                    } catch (Exception e) {
                        System.err.println("Improper choice\n");
                        break;
                    }
                    break;

                case 4:
                    int i,
                     initialBullsNumber = bulls.length;
                    BullDetails[] newBulls = new BullDetails[initialBullsNumber + 1];
                    bullRequests = new BullRequests();
                    for (i = 0; i < initialBullsNumber; i++) {
                        newBulls[i] = bulls[i];
                    }
                    newBulls[i] = new BullDetails(bullRequests.addBull());
                    newBulls[i].setAlive(true);

                    setBulls(newBulls);

                    for (int j = 0; j < bulls.length; j++) {
                        bulls[j].getServer().setBulls(bulls);
                    }
                    new Thread(bulls[i].getServer()).start();

                    bulls[i].getServer().setBulls(bulls);
                    bulls[i].getServer().initiateElection();

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        System.err.println("Interrupted\n");
                    }
                    break;
                case 5:
                    System.out.print("\nWhich bull(process) do you want to revive"
                            + "\n\t(Choose between: " + getDeadChoices() + ")\n\t>> ");
                    try {
                        choice = new Scanner(System.in).nextInt();
                        System.out.println();
                        if (choice > 9) {
                            BullDetails bull = getBull(choice, bulls);
                            bull.setAlive(true);
                            System.out.println("\033[34;2mSwitchbox: Bull " + bull.getId() + " is up\n");
                            bull.getServer().initiateElection();
                            Thread.sleep(2000);
                        } else if (choice >= 1 && choice <= 6) {
                            choice -= 1;
                            bulls[choice].setAlive(true);
                            System.out.println("\033[34;2mSwitchbox: Bull " + bulls[choice].getId() + " is up\n");
                            bulls[choice].getServer().initiateElection();
                            Thread.sleep(2000);
                        } else {
                            System.err.println("Improper choice\n");
                            break;
                        }
                    } catch (Exception e) {
                        System.err.println("Improper choice\n");
                        break;
                    }
                    break;
                case 6:
                    BullDetails coordinator = Server.getCoordinator();
                    if (coordinator != null && coordinator.isAlive()) {
                        System.out.println("\033[36;2mSwitchbox: Bull " + coordinator.getId() + " is the coordinator");
                    } else {
                        System.out.println("\033[31;2mSwitchbox: Coordinator is down\n");
                    }
                    break;
                default:
                    System.err.println("Improper choice\n");
                    break;
            }

        }

    }

    /**
     * @return the bulls
     */
    public static BullDetails[] getBulls() {
        return bulls;
    }

    /**
     * @param aBulls the bulls to set
     */
    public static void setBulls(BullDetails[] aBulls) {
        bulls = aBulls;
    }

    private static BullDetails getBull(int bullId, BullDetails[] bulls) {

        for (BullDetails bull : bulls) {
            if (bull.getId() == bullId) {
                return bull;
            }
        }
        return null;
    }

    private static BullDetails[] bulls;

    private static String getLiveChoices() {
        String choices = "";
        for (int i = 0; i < bulls.length; i++) {
            if (bulls[i].isAlive()) {
                choices += " " + (i + 1);
            }
        }
        return choices;
    }

    private static String getDeadChoices() {
        String choices = "";
        for (int i = 0; i < bulls.length; i++) {
            if (!bulls[i].isAlive()) {
                choices += " " + (i + 1);
            }
        }
        return choices;
    }

}
