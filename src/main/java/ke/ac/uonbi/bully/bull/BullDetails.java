/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.ac.uonbi.bully.bull;

import ke.ac.uonbi.bully.communication.Server;
import ke.ac.uonbi.bully.switchbox.Switch;

/**
 *
 * @author siech
 */
public class BullDetails {

    private boolean alive;
    private final int id;
    private final Server server;
    private final BullDetails[] bulls;

    public BullDetails(int id) {
        this.bulls = Switch.getBulls();

        this.id = id;
        this.server = new Server(id, bulls);
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the server
     */
    public Server getServer() {
        return server;
    }

    /**
     * @return the bulls
     */
    public BullDetails[] getBulls() {
        return bulls;
    }

    /**
     * @return the alive
     */
    public boolean isAlive() {
        return alive;
    }

    /**
     * @param alive the alive to set
     */
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void kill() {
        setAlive(false);
    }
}
