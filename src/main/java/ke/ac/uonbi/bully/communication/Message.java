/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.ac.uonbi.bully.communication;

/**
 *
 * @author siech
 */
public enum Message {
    ELECTION, OK, I_WON, CHECK_COORDINATOR, COORDINATOR_UP;

    public static String toString(Message message) {
        switch (message) {
            case ELECTION:
                return "election";
            case OK:
                return "okay";
            case I_WON:
                return "i won";
            case CHECK_COORDINATOR:
                return "check coordinator";
            case COORDINATOR_UP:
                return "coordinator up";
            default:
                return null;
        }
    }

    public static Message getMessage(String message) {
        switch (message) {
            case "election":
                return ELECTION;
            case "okay":
                return OK;
            case "i won":
                return I_WON;
            case "check coordinator":
                return CHECK_COORDINATOR;
            case "ordinator up":
                return COORDINATOR_UP;
            default:
                return null;
        }
    }

}
