/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.ac.uonbi.bully.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Random;
import ke.ac.uonbi.bully.bull.BullDetails;
import ke.ac.uonbi.bully.entities.Election;
import ke.ac.uonbi.bully.requests.CoordinatorRequests;

/**
 *
 * @author siech
 */
public class Server implements Runnable {

    public Server(int bullId, BullDetails[] bulls) {
        this.thisBullId = bullId;
        this.bulls = bulls;

        iWon = true;
        try {

            port = new Random().nextInt(13000) + 13000;
            socket = new DatagramSocket(port);

        } catch (SocketException e) {
            System.err.println("Bull " + thisBullId + ": Socket not created");
        }

    }

    @Override
    public void run() {

        while (true) {

            try {

                byte[] byteBuffer = new byte[256];

                DatagramPacket packet = new DatagramPacket(byteBuffer, byteBuffer.length);
                getSocket().receive(packet);

                String dataString = new String(packet.getData(), 0, packet.getLength());
                String[] dataArray = dataString.split(",");
                int otherBullId = Integer.parseInt(dataArray[0]);
                setMessage(Message.getMessage(dataArray[1]));
                int electionId = 0;
                try {
                    electionId = Integer.parseInt(dataArray[2]);
                } catch (Exception e) {
                }

                switch (getMessage()) {
                    case ELECTION:
                        System.out.println("Bull " + getThisBullId() + ": Bull " + otherBullId + " has initiated election " + electionId);

                        sendOkMessage(getBull(otherBullId, bulls), electionId);

                        BullDetails largest = null;
                        for (int i = getBulls().length - 1; i >= 0; i--) {
                            if (getBulls()[i].isAlive()) {
                                largest = getBulls()[i];
                                break;
                            }
                        }
                        if (largest != null && largest.getId() - 1 == otherBullId) {
                            iWon = true;
                        } else {
                            iWon = false;
                            initiateElection();
                        }

                        if (iWon) {
                            new CoordinatorRequests().setCoordinator(getThisBullId(), getElection().getId());
                            setCoordinator(getBull(thisBullId, bulls));
                            System.out.println("\n\033[36;3mBull " + getThisBullId() + ": I am the coordinator\n");
                            for (int i = 0; i < getThisBullId() - getBulls()[0].getId(); i++) {
                                if (getBulls()[i] != null && getBulls()[i].isAlive()) {
                                    sendIWonMessage(getBulls()[i]);
                                }
                            }
                        }
                        break;
                    case OK:
                        if (getElection() != null && electionId == getElection().getId()) {
                            //other process taken over, my job is done
                            System.out.println("Bull " + getThisBullId() + ": Bull " + otherBullId + " has okayed on election " + electionId);
                        }
                        break;
                    case I_WON:
                        System.out.println("Bull " + getThisBullId() + ": Bull " + otherBullId + " has won on election " + electionId);
                        break;
                }

            } catch (IOException e) {
                System.err.println("Bull " + getThisBullId() + ": IOException occurred");
            }

        }
    }

    public void initiateElection() {

        setElection(new CoordinatorRequests().startElection());
        System.out.println("Bull " + getThisBullId() + ": I have initiated election " + getElection().getId());
        /**
         * Send election message to processes with greater ids
         */
        for (int i = getBull(thisBullId, bulls).getId() - getBulls()[0].getId() + 1; i < getBulls().length; i++) {
            if (getBulls()[i] != null && getBulls()[i].isAlive()) {
                sendElectionMessage(getBulls()[i]);
            }
        }

        BullDetails largest = null;
        for (int i = getBulls().length - 1; i >= 0; i--) {
            if (getBulls()[i].isAlive()) {
                largest = getBulls()[i];
                break;
            }
        }
        if (largest != null && largest.getId() == getThisBullId()) {
            iWon = true;
        }
        if (iWon) {
            System.out.println();
            new CoordinatorRequests().setCoordinator(getThisBullId(), getElection().getId());
            setCoordinator(getBull(thisBullId, bulls));
            System.out.println("\n\033[36;3mBull " + getThisBullId() + ": I am the coordinator\n");
            for (int i = 0; i < getThisBullId() - getBulls()[0].getId(); i++) {
                if (getBulls()[i] != null && getBulls()[i].isAlive()) {
                    sendIWonMessage(getBulls()[i]);
                }
            }
        }
    }

    private void sendElectionMessage(BullDetails bull) {
        System.out.println("Bull " + getThisBullId() + ": Sending election message to bull " + bull.getId());

        DatagramSocket clientSocket;
        try {
            clientSocket = new DatagramSocket();
        } catch (SocketException ex) {
            System.err.println("Socket could not be created");
            return;
        }

        byte[] byteBuffer;
        InetAddress address;
        try {
            address = Inet4Address.getByName("localhost");
        } catch (UnknownHostException ex) {
            System.err.println("The specified host is unknown");
            return;
        }

        String dataString = getThisBullId() + "," + Message.toString(Message.ELECTION) + "," + getElection().getId();
        byteBuffer = dataString.getBytes();

        int otherPort = bull.getServer().getPort();
        DatagramPacket packet = new DatagramPacket(byteBuffer, byteBuffer.length, address, otherPort);
        try {
            clientSocket.send(packet);
        } catch (IOException ex) {
            System.err.println("Bull " + getThisBullId() + ": Could not send election message to bull " + bull.getId() + " on election " + getElection().getId());
        }

        if (getElection() == null) {
            setElection(new CoordinatorRequests().startElection());
        }

    }

    private void sendOkMessage(BullDetails bull, int electionId) {
        System.out.println("Bull " + getThisBullId() + ": Sending ok message to bull " + bull.getId());

        byte[] byteBuffer;
        DatagramPacket packet;

        String dataString = getThisBullId() + "," + Message.toString(Message.OK) + "," + electionId;
        byteBuffer = dataString.getBytes();

        InetAddress address;
        try {
            address = Inet4Address.getByName("localhost");
        } catch (UnknownHostException ex) {
            System.err.println("The specified host is unknown");
            return;
        }

        int otherPort = bull.getServer().getPort();
        packet = new DatagramPacket(byteBuffer, byteBuffer.length, address, otherPort);
        try {
            getSocket().send(packet);
        } catch (IOException ex) {
            System.err.println("Bull " + getThisBullId() + ": Could not send ok message to bull " + bull.getId() + " on election " + getElection().getId());
        }
    }

    private void sendIWonMessage(BullDetails bull) {
        System.out.println("Bull " + getThisBullId() + ": Sending i won message to bull " + bull.getId());

        DatagramSocket clientSocket;
        try {
            clientSocket = new DatagramSocket();
        } catch (SocketException ex) {
            System.err.println("Socket could not be created");
            return;
        }

        byte[] byteBuffer;
        InetAddress address;
        try {
            address = Inet4Address.getByName("localhost");
        } catch (UnknownHostException ex) {
            System.err.println("The specified host is unknown");
            return;
        }

        String dataString = getThisBullId() + "," + Message.toString(Message.I_WON) + "," + getElection().getId();
        byteBuffer = dataString.getBytes();

        int otherPort = bull.getServer().getPort();
        DatagramPacket packet = new DatagramPacket(byteBuffer, byteBuffer.length, address, otherPort);
        try {
            clientSocket.send(packet);
        } catch (IOException ex) {
            System.err.println("Bull " + getThisBullId() + ": Could not send i won message to bull " + bull.getId() + " on election " + getElection().getId());
        }

    }

    public class Coordinator implements Runnable {

        @Override
        public void run() {
            while (getBull(thisBullId, bulls).isAlive()) {
                checkCoordinator();
            }
        }

    }

    private void checkCoordinator() {
        if (getCoordinator() == null) {
            return;
        }
//        System.out.println("Bull " + getThisBullId() + ": Sending check coordinator message to bull " + getCoordinator().getId());

        DatagramSocket clientSocket;

        try {
            clientSocket = new DatagramSocket(getCoordinatorPort());
        } catch (SocketException ex) {
            setCoordinatorPort(new Random().nextInt(27000) + 27000);
            try {
                clientSocket = new DatagramSocket(getCoordinatorPort());
            } catch (SocketException e) {
                System.err.println("Socket could not be created " + ex);
                return;
            }
        }

        byte[] byteBuffer;
        InetAddress address;
        try {
            address = Inet4Address.getByName("localhost");
        } catch (UnknownHostException ex) {
            System.err.println("The specified host is unknown");
            return;
        }

        String dataString = getThisBullId() + "," + Message.toString(Message.CHECK_COORDINATOR) + ", null";
        byteBuffer = dataString.getBytes();

        int otherPort = getCoordinator().getServer().getPort();
        DatagramPacket packet = new DatagramPacket(byteBuffer, byteBuffer.length, address, otherPort);

        try {
            clientSocket.send(packet);
        } catch (IOException ex) {
            System.err.println("Bull " + getThisBullId() + ": Could not send check coordinator message to bull " + getCoordinator().getId());
        }

        packet = new DatagramPacket(byteBuffer, byteBuffer.length);
        try {
            clientSocket.setSoTimeout(5000);
            clientSocket.receive(packet);
        } catch (SocketTimeoutException ex) {
            System.err.println("Bull " + getThisBullId() + ": Coordinator(Bull " + getCoordinator().getId() + ") is down");
            initiateElection();
        } catch (IOException ex) {
            System.err.println("Bull " + getThisBullId() + ": Could not read from socket");
        }

        dataString = new String(packet.getData(), 0, packet.getLength());
        String[] dataArray = dataString.split(",");
        Message coordinatorMessage = Message.getMessage(dataArray[1]);

        if (coordinatorMessage == Message.COORDINATOR_UP) {
            System.out.println(coordinatorMessage);
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
                System.err.println("Interrupted");
            }
        }
    }

    private void coordinatorUp(BullDetails bull) {
        System.out.println("Bull " + getThisBullId() + ": Sending coordinator up message to bull " + bull.getId());

        byte[] byteBuffer;
        DatagramPacket packet;

        String dataString = getThisBullId() + "," + Message.toString(Message.COORDINATOR_UP) + ", null";
        byteBuffer = dataString.getBytes();

        InetAddress address;
        try {
            address = Inet4Address.getByName("localhost");
        } catch (UnknownHostException ex) {
            System.out.println("Bull " + getThisBullId() + ": The specified host is unknown");
            return;
        }

        int otherPort = bull.getServer().getCoordinatorPort();
        packet = new DatagramPacket(byteBuffer, byteBuffer.length, address, otherPort);
        try {
            getSocket().send(packet);
        } catch (IOException ex) {
            System.err.println("Bull " + getThisBullId() + ": Could not send coordinator up message to bull " + bull.getId());
        }
    }

//<editor-fold defaultstate="collapsed" desc="setters and getters"> 
    /**
     * @return the coordinatorPort
     */
    public int getCoordinatorPort() {
        return coordinatorPort;
    }

    /**
     * @param coordinatorPort the coordinatorPort to set
     */
    public void setCoordinatorPort(int coordinatorPort) {
        this.coordinatorPort = coordinatorPort;
    }

    /**
     * @return the socket
     */
    public DatagramSocket getSocket() {
        return socket;
    }

    /**
     * @param socket the socket to set
     */
    public void setSocket(DatagramSocket socket) {
        this.socket = socket;
    }

    /**
     * @return the bufferedReader
     */
    public BufferedReader getBufferedReader() {
        return bufferedReader;
    }

    /**
     * @param bufferedReader the bufferedReader to set
     */
    public void setBufferedReader(BufferedReader bufferedReader) {
        this.bufferedReader = bufferedReader;
    }

    /**
     * @return the message
     */
    public Message getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(Message message) {
        this.message = message;
    }

    /**
     * @return the thisBullId
     */
    public int getThisBullId() {
        return thisBullId;
    }

    /**
     * @return the bulls
     */
    public BullDetails[] getBulls() {
        return bulls;
    }

    /**
     * @param bulls the bulls to set
     */
    public void setBulls(BullDetails[] bulls) {
        this.bulls = bulls;
    }

    /**
     * @return the election
     */
    public Election getElection() {
        return election;
    }

    /**
     * @param election the election to set
     */
    public void setElection(Election election) {
        this.election = election;
    }

    /**
     * @return the port
     */
    public int getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(int port) {
        this.port = port;
    }

    private BullDetails getBull(int otherBullId, BullDetails[] bulls) {

        for (BullDetails bull : bulls) {
            if (bull.getId() == otherBullId) {
                return bull;
            }
        }
        return null;
    }

    /**
     * @return the coordinator
     */
    public static BullDetails getCoordinator() {
        return coordinator;
    }

    /**
     * @param coordinator the coordinator to set
     */
    public void setCoordinator(BullDetails coordinator) {
        this.coordinator = coordinator;
    }

//</editor-fold>
    private BufferedReader bufferedReader;
    private static BullDetails coordinator;
    private DatagramSocket socket;
    private final int thisBullId;
    private int coordinatorPort;
    private BullDetails[] bulls;
    private Election election;
    private Message message;
    private boolean iWon;
    private int port;

}
